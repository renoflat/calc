import Vue from 'vue';
import Router from 'vue-router';
import CalculatorPage from './views/CalculatorPage';
import AdminPage from './views/AdminPage';
import Works from './views/Works';
import WorkPage from './views/WorkPage';
import MaterialPage from './views/MaterialPage';
import Materials from './views/Materials';
import Stages from './views/Stages';
import StageCreate from './components/StageCreate';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/admin',
      name: 'admin',
      component: AdminPage,
      children: [
        {
          path: 'works',
          name: 'works',
          component: Works,
        },
        {
          path: 'work/:id/edit',
          name: 'work_edit',
          component: WorkPage
        },
        {
          path: 'work/create',
          name: 'work_create',
          component: WorkPage
        },
        {
          path: 'materials',
          name: 'materials',
          component: Materials,
        },
        {
          path: 'material/:id/edit',
          name: 'material_edit',
          component: MaterialPage
        },
        {
          path: 'material/create',
          name: 'material_create',
          component: MaterialPage
        },
        {
          path: 'stages',
          name: 'stages',
          component: Stages
        },
        {
          path: 'stages/:id/edit',
          name: 'stage_edit',
          component: StageCreate
        },
        {
          path: 'stage/create',
          name: 'stage_create',
          component: StageCreate
        },
      ]
    },
    {
      path: '/',
      name: 'calc',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: CalculatorPage,
    },

    {
      path: '/calc/:id',
      name: 'calc_saved',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: CalculatorPage,
    },
  ],
});
